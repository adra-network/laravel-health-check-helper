# Changelog

All notable changes to `laravel-health-check-helper` will be documented in this file

## 1.0.0 - 201X-XX-XX

- initial release
