<?php

namespace Adranetwork\LaravelHealthCheckHelper\Tests;

use Spatie\Health\HealthServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{

    protected function getPackageProviders($app)
    {
        return [
            HealthServiceProvider::class,
        ];
    }


    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);

        $app['config']->set('health.result_stores', [
            \Spatie\Health\ResultStores\InMemoryHealthResultStore::class
        ]);

    }


}
