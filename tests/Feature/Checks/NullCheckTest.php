<?php

namespace Adranetwork\LaravelHealthCheckHelper\Tests\Feature\Checks;

use Adranetwork\LaravelHealthCheckHelper\Checks\NullCheck;
use Adranetwork\LaravelHealthCheckHelper\Tests\TestCase;
use Spatie\Health\Checks\Result;
use Spatie\Health\Enums\Status;

class NullCheckTest extends TestCase
{
    /** @test **/
    public function it_returns_ok ()
    {
        $result = NullCheck::new()
            ->run();
        $this->assertInstanceOf(Result::class, $result);
        $this->assertEquals($result->status, Status::ok());
    }
}
