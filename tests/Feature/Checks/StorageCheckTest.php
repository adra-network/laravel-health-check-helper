<?php

namespace Adranetwork\LaravelHealthCheckHelper\Tests\Feature\Checks;

use Adranetwork\LaravelHealthCheckHelper\Checks\NullCheck;
use Adranetwork\LaravelHealthCheckHelper\Checks\StorageCheck;
use Adranetwork\LaravelHealthCheckHelper\Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Spatie\Health\Checks\Result;
use Spatie\Health\Enums\Status;

class StorageCheckTest extends TestCase
{
    /** @test **/
    public function it_uses_s3_driver_by_default ()
    {
        $check = StorageCheck::new();
        $this->assertEquals('s3', $check->disk);

    }

    /** @test **/
    public function it_returns_ok ()
    {
        Storage::fake('s3');
        $result = StorageCheck::new()->run();
        $this->assertInstanceOf(Result::class, $result);
        $this->assertEquals($result->status, Status::ok());
    }

    /** @test **/
    public function it_returns_failed ()
    {
        // here we dont fake the Storage so it'll throw an error
        $result = StorageCheck::new()->run();
        $this->assertInstanceOf(Result::class, $result);
        $this->assertEquals($result->status, Status::failed());
    }
}
