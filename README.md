# Helpers to Extend Spatie Health Check package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/adranetwork/laravel-health-check-helper.svg?style=flat-square)](https://packagist.org/packages/adranetwork/laravel-health-check-helper)
[![Total Downloads](https://img.shields.io/packagist/dt/adranetwork/laravel-health-check-helper.svg?style=flat-square)](https://packagist.org/packages/adranetwork/laravel-health-check-helper)

This packages provides extra check to extends Spatie Health Packages
## Installation

You can install the package via composer:

```bash
composer require adranetwork/laravel-health-check-helper
```


### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email francois.auclair.911@gmail.com instead of using the issue tracker.

## Credits

-   [Francois Auclair](https://github.com/adranetwork)
-   [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).
