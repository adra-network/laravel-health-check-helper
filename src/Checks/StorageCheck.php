<?php

namespace Adranetwork\LaravelHealthCheckHelper\Checks;

use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Spatie\Health\Checks\Check;
use Spatie\Health\Checks\Result;

class StorageCheck extends Check
{
    public string $disk;
    protected string $file = 'health-check.txt';
    protected string $content;

    public static function new(): static
    {
        $instance = new static();

        $instance->content(Str::random(32));

        $instance->disk('s3');

        $instance->everyMinute();

        return $instance;
    }

    public function content(string $content): self
    {
        $this->content = $content;
        return $this;
    }
    public function disk(string $disk): self
    {
        $this->disk = $disk;
        return $this;
    }

    public function run(): Result
    {
        try {
            $result = Result::make();
            Storage::disk($this->disk)->put(
                $this->file,
                $this->content
            );

            $contents = Storage::disk($this->disk)->get(
                 $this->file
            );

            Storage::disk($this->disk)->delete( $this->file);

            if ($contents !== $this->content) {
                $result->failed('Amazon S3 connection is failing');
                return $result;
            }
            return $result->ok();
        } catch (Exception $exception) {
            report($exception);
            $result = Result::make();
            return $result->failed($exception->getMessage());
        }
    }
}
